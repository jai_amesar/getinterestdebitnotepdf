const puppeteer = require("puppeteer");
var logger = require("../helpers/log4j").logger;


const printPdf = (pupData, pdfNameWop, uuid) =>{
    return new Promise(async (resolve, reject) => {
        try {
            logger.debug('Starting: Generating PDF Process, Kindly wait ..');
            /** Launch a headleass browser */
            const browser = await puppeteer.launch({ headless: true, args: ['--disable-dev-shm-usage', '--no-sandbox', '--disable-setuid-sandbox'] });
            
            /* 1- Ccreate a newPage() object. It is created in default browser context. */
            const page = await browser.newPage();
            
            /* 2- Will open our generated `.html` file in the new Page instance. */
            //await page.goto(pupData, { waitUntil: 'networkidle0' });
            await page.setContent(pupData);
            await page.emulateMedia('screen');
            
            /* 3- Take a snapshot of the PDF */
            await page.pdf({
                format: 'A4',
                margin: {
                    top: '10px',
                    right: '10px',
                },
                    bottom: '10px',
                    left: '10px',
                path: pdfNameWop
            });
            
            /* 4- Cleanup: close browser. */
            await browser.close();
            logger.debug('Ending: Generating PDF Process');
            logger.info(uuid, "interestResetLetterROI ", "Success ", JSON.stringify({ "successMessage": "Generating PDF Process" }));
            resolve(true);
        }
        catch (e) {
            reject(e)
        }
    })
};

module.exports = {
    printPdf : printPdf 
}

