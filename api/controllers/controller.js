var queryBuilder = require("../helpers/queryBuilder");
var oracledb = require('oracledb');
var async = require("async");
var fs = require('fs');
var log4js = require("log4js");
const { v4: uuidv4 } = require('uuid');
var path = require("path");
var moment = require("moment");
var ejs = require("ejs");
var GeneratePDF = require("./GeneratePDF");
var ABFLHeader = require("../../ABFLHeader").header;
var ABFLFooter = require("../../ABFLFooter").footer;
const pdf2base64 = require('pdf-to-base64');
const HummusRecipe = require('hummus-recipe');
const crypto = require('crypto');
var GeneratePDF = require("./GeneratePDF");
var logger = require("../helpers/log4j").logger;

oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

// Interest Debit Note View API
var getInterestDebitNoteReport = function (req, res) {
    try {
        var uuid = uuidv4();
        logger.info(uuid, "getInterestDebitNotePdf ", "Request ", JSON.stringify(req.body));
        var params = req.body;
        
        if (params && params.filter) {
            if (!params.filter.AGREEMENTNO || 0 === params.filter.AGREEMENTNO.length) {
                throw new Error('Agreement no. is missing');
            }
        }
        else {
            throw new Error('filter is missing');
        }

        var view = process.env.DATAMART_SCHEMA + "." + process.env.INTEREST_DEBIT_NOTE;
        var schemaUser = process.env.DATAMART_USER;
        var dateFields = ["CBD"];
        var reportType = params.reportType;
        var encryption = params.encryption;
        var PDFPassword = params.pdfPassword;
        params.filter = findAndReplaceProp(params.filter, 'gte', '$gte');
        params.filter = findAndReplaceProp(params.filter, 'lte', '$lte');
        params.filter = findAndReplaceProp(params.filter, 'gt', '$gt');
        params.filter = findAndReplaceProp(params.filter, 'lt', '$lt');

        commonProcessing(params, view, schemaUser, uuid).then((data) => {
            var pdfData = "";
            async.eachSeries(data[0].data, (datum, datumCallback) => {
                async.eachSeries(dateFields, (date, dateCallback) => {
                    if (datum[date]) {
                        datum[date] = datum[date];
                    }
                    dateCallback();
                }, function () {
                    datumCallback();
                })
            }, function () {
                if (reportType.toLowerCase() == "pdf") {
                    var interestReceivableAmt = 0;
                    if (data[0].data.length > 0 && data[1].length > 0) {
                        async.eachSeries(data[2], function (eachElement, callback) {
                            interestReceivableAmt += eachElement.INTERESTAMT;
                            var momentObj = moment(new Date(eachElement.PROCESSDATE));
                            var momentString = momentObj.format('DD-MMM-YYYY');
                            eachElement.PROCESSDATE = momentString;
                            callback();
                        }, function (err) {
                            if (err)
                                throw new Error(err);
                        });

                        var fromdate = params.filter.CBD["$gte"];
                        var toDate = params.filter.CBD["$lte"];

                        var momentObj_toDate = moment(new Date(toDate));
                        var momentObj_fromDate = moment(new Date(fromdate));

                        var formattedToDate_1 = momentObj_toDate.format('Do MMM YYYY');
                        var formattedToDate_2 = momentObj_toDate.format('DD-MMM-YYYY');
                        var formattedFromDate = momentObj_fromDate.format('DD-MMM-YYYY');

                        var AmountInWords = RsPaise(Math.round(interestReceivableAmt * 100) / 100);

                        ejs.renderFile(path.join(__dirname, '../../views/', 'InterestDebitNote.ejs'), {
                            InterestDebitData: data[0].data,
                            AddressData: data[1],
                            DayWiseData: data[2],
                            fromDateForDaywise: data[0].dates.fromDateForDaywise,
                            toDateForDaywise: data[0].dates.toDateForDaywise,
                            CBDForDebitNote: moment(data[0].dates.CBDForDebitNote).format("Do MMM YYYY"),
                            ToDate: formattedToDate_1,
                            ToDate_Daywise: formattedToDate_2,
                            interestReceivableAmt: interestReceivableAmt,
                            FromDate: formattedFromDate,
                            InterestDebitAmtInWords: AmountInWords,
                            ABFLHeader: ABFLHeader,
                            ABFLFooter: ABFLFooter
                        }, (err, ejsData) => {
                            if (err) {
                                throw new Error(err);
                            }
                            else {
                                var pdfNameWop = uuid + '_wop.pdf';
                                var pdfName = uuid + '.pdf';

                                GeneratePDF.printPdf(ejsData, pdfNameWop, uuid).then(() => {

                                    var pdfFilePathWop = "./" + pdfNameWop;
                                    var pdfFilePath = "./" + pdfName;
                                    var pdfFilePathForBase64 = pdfFilePathWop;

                                    // Password protect generated PDF
                                    if (params.passwordProtectPDF && params.passwordProtectPDF.toLowerCase() == "true") {
                                        if (encryption && encryption.toLowerCase() == "true") {
                                            PDFPassword = decrypt(PDFPassword);
                                        }
                                        const pdfDoc = new HummusRecipe(path.join(__dirname + '../../../' + pdfNameWop), path.join(__dirname + '../../../' + pdfName));

                                        pdfDoc
                                            .encrypt({
                                                userPassword: PDFPassword,
                                                ownerPassword: '456',
                                                userProtectionFlag: 4
                                            })
                                            .endPDF();
                                        pdfFilePathForBase64 = pdfFilePath;
                                    }

                                    pdf2base64(pdfFilePathForBase64)
                                        .then(
                                            (response) => {
                                                pdfData = response;
                                                logger.info(uuid, "getInterestDebitNotePdf ", "Response ", JSON.stringify({ "dataLength": data[0].data.length, "PDFData": true }));
                                                res.json({ getInterestDebitNoteReport: data[0].data, status: { Status: "Success", Code: 200 }, PDFData: pdfData });
                                                deleteFile(params, pdfName, uuid, pdfNameWop);
                                            }
                                        )
                                        .catch(
                                            (error) => {
                                                logger.error(uuid, "getInterestDebitNotePdf", "Error", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                                                res.status(400).json({ getInterestDebitNoteReport: [], status: { Status: "Error", Code: 400, DBErrorCode: error ? error.errorNum : undefined, ErrorDescription: error ? error.message : undefined } });
                                                deleteFile(params, pdfName, uuid, pdfNameWop);
                                            }
                                        )
                                }).catch((error) => {
                                    logger.error(uuid, "getInterestDebitNotePdf", "Error", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                                    res.status(400).json({ getInterestDebitNoteReport: [], status: { Status: "Error", Code: 400, DBErrorCode: error ? error.errorNum : undefined, ErrorDescription: error ? error.message : undefined } });
                                });

                            }
                        });

                    }
                    else {
                        logger.info(uuid, "getInterestDebitNotePdf ", "Response ", JSON.stringify({ "dataLength": data[0].data.length, "PDFData": false }));
                        res.json({ getInterestDebitNoteReport: data[0].data, status: { Status: "Success", Code: 200 }, PDFData: null });
                    }
                }

                else if (reportType.toLowerCase() == "data") {
                    logger.info(uuid, "getInterestDebitNotePdf ", "Response ", JSON.stringify({ "dataLength": data[0].data.length, "PDFData": false }));
                    res.json({ getInterestDebitNoteReport: data[0].data, status: { Status: "Success", Code: 200 }, PDFData: null })
                }
            })
        })
            .catch(err => {
                logger.error(uuid, "getInterestDebitNotePdf ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                res.status(400).json({ getInterestDebitNoteReport: [], status: { Status: "Error", Code: 400, DBErrorCode: err ? err.errorNum : undefined, ErrorDescription: err ? err.message : undefined } });
            })
    }
    catch (err) {
        logger.error(uuid, "getInterestDebitNotePdf ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
        res.status(400).json({ getInterestDebitNoteReport: [], status: { Status: "Error", Code: 400, DBErrorCode: err ? err.errorNum : undefined, ErrorDescription: err ? err.message : undefined } });
    }
}

// Common processing module that builds the SQL query from the JSON filter
function commonProcessing(params, view, schemaUser, uuid) {
    return new Promise((resolve, reject) => {
        makeOracleConnection(schemaUser, uuid).then(connection => {
            var toDate, fromDate;
            async.series([
                function (callback) {
                    var debitNoteParams = {};
                    debitNoteParams["filter"] =
                    {
                        "AGREEMENTNO": params.filter.AGREEMENTNO,
                    };
                    let query = queryBuilder(debitNoteParams, 'select', view);
                    logger.info(uuid, "getInterestDebitNotePdf ", "Query ", JSON.stringify({ "query": JSON.stringify(query.query), "value": JSON.stringify(query.values) }));
                    var startTime = Date.now();
                    connection.execute(query.query, query.values, (err, result) => {
                        if (err) {
                            callback(err);
                        } else {
                            var responseTime = ((Date.now() - startTime) / 1000);
                            if (result.rows.length > 0) {
                                logger.info(uuid, "getInterestDebitNotePdf ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                calculateProcessDateFilter(result.rows).then(setDates => {
                                    if (setDates) {
                                        toDate = setDates.toDateForDaywise;
                                        fromDate = setDates.fromDateForDaywise;
                                        callback(null, { data: result.rows, dates: setDates });
                                    } else {
                                        callback(err);
                                    }
                                })
                            }
                            else {
                                callback(new Error('No data found...'));
                            }
                        }
                    });
                },
                function (callback) {
                    var addressView = process.env.DATAMART_SCHEMA + "." + process.env.CUSTOMER_ACCOUNT_INFO;
                    var mailingAddress = "Y";
                    var addparams = {};
                    addparams["filter"] =
                    {
                        "AGREEMENTNO": params.filter.AGREEMENTNO
                    };
                    let query = queryBuilder(addparams, 'select', addressView);
                    logger.info(uuid, "getInterestDebitNotePdf ", "Query ", JSON.stringify({ "query": JSON.stringify(query.query), "value": JSON.stringify(query.values) }));
                    var startTime = Date.now();
                    connection.execute(query.query, query.values, (err, result) => {
                        if (err) {
                            callback(err);

                        } else {
                            var responseTime = ((Date.now() - startTime) / 1000);
                            logger.info(uuid, "getInterestDebitNotePdf ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                            callback(null, result.rows);
                        }
                    });
                },
                function (callback) {
                    var addparams = {};
                    addparams["filter"] =
                    {
                        "AGREEMENTNO": params.filter.AGREEMENTNO,
                        "PROCESSDATE": {
                            "$gte": fromDate,
                            "$lte": toDate
                        }
                    };

                    async.parallel({
                        interestDaywiseBreakup: function (callback1) {
                            var dayWiseView = process.env.DATAMART_SCHEMA + "." + process.env.INTEREST_DAYWISE_BREAKUP;
                            let query = queryBuilder(addparams, 'select', dayWiseView);
                            logger.info(uuid, "getInterestDebitNotePdf ", "Query ", JSON.stringify({ "query": JSON.stringify(query.query), "value": JSON.stringify(query.values) }));
                            var startTime = Date.now();
                            connection.execute(query.query, query.values, (err, result) => {
                                if (err) {
                                    callback1(err);

                                } else {
                                    var responseTime = ((Date.now() - startTime) / 1000);
                                    logger.info(uuid, "getInterestDebitNotePdf ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                    callback1(null, result.rows);
                                }
                            })

                        },

                        dailyPrinDebitNote: function (callback2) {
                            var dayWiseView = process.env.DATAMART_SCHEMA + "." + process.env.DAILY_DEBIT_NOTE;
                            let query = queryBuilder(addparams, 'select', dayWiseView);
                            logger.info(uuid, "getInterestDebitNotePdf ", "Query ", JSON.stringify({ "query": JSON.stringify(query.query), "value": JSON.stringify(query.values) }));
                            var startTime = Date.now();
                            connection.execute(query.query, query.values, (err, result) => {
                                if (err) {
                                    callback2(err);
                                } else {

                                    var responseTime = ((Date.now() - startTime) / 1000);
                                    logger.info(uuid, "getInterestDebitNotePdf ", "Success ", JSON.stringify({ 'dataLength': result.rows.length, 'responseTime(in S)': responseTime }));
                                    callback2(null, result.rows);
                                }
                            })
                        }

                    }, function (err, results) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            let interest = results['interestDaywiseBreakup'];
                            let daily = results['dailyPrinDebitNote'];
                            let finalResp = [];
                            if (interest.length > 0) {
                                for (var i = 0; i < interest.length; i++) {
                                    let record = {};
                                    Object.assign(record, interest[i]);
                                    let amt = 0;
                                    for (var j = 0; j < daily.length; j++) {
                                        if (moment(interest[i].PROCESSDATE).isSame(moment(daily[j].PROCESSDATE, 'day'))) {
                                            amt = daily[j].PRINCE_DUE_OUT_DEBIT_NOTE;
                                        }
                                    }
                                    record.OUTSTANDING = amt;
                                    finalResp.push(record);
                                }
                            }
                            callback(null, finalResp);
                        }

                    });
                }
            ], function (err, results) {
                connection.close();
                if (err) {
                    reject(err);
                }
                else {
                    resolve(results);
                }

            });

        })
            .catch(err => {
                reject(err);
            })
    })
}

// Oracle connection module
function makeOracleConnection(schemaUser, uuid) {
    return new Promise((resolve, reject) => {
        var connectionDetails = {
            user: schemaUser,
            password: process.env.ORACLE_PASSWORD,
            connectString: process.env.ORACLE_CONNECTION_STRING
        }
        oracledb.getConnection(connectionDetails,
            function (err, connection) {
                if (err) {
                    reject(err);
                }
                else {
                    logger.info(uuid, "getInterestDebitNotePdf ", "Success ", JSON.stringify({ 'successMessage': "Application started -> Connected to DB" }));
                    resolve(connection);
                }
            });
    })
}

// Delete PDF from Kubenates once generated 
function deleteFile(params, pdfName, uuid, pdfNameWop) {
    if (params.passwordProtectPDF && params.passwordProtectPDF.toLowerCase() == "true") {
        fs.unlink(pdfName, (err) => {
            if (err) {
                logger.error(uuid, "getInterestDebitNotePdf ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
                return;
            }
        });
    }

    fs.unlink(pdfNameWop, (err) => {
        if (err) {
            logger.error(uuid, "getInterestDebitNotePdf ", "Error ", JSON.stringify({ "errorDescription": err ? err.message : undefined }));
            return;
        }
    });
}

// Password decryption utility
function decrypt(text) {
    try {
        let encryptedText = Buffer.from(text, 'base64');
        let decipher = crypto.createDecipheriv("aes-256-ecb", Buffer.from(process.env.ENCRYPTION_KEY), '');
        let decrypted = decipher.update(encryptedText);

        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }
    catch (err) {
        throw new Error(err);
    }
}

// Find and replace gte/lte with $gte/$lte
function findAndReplaceProp(obj, key, replaceKey, out) {
    var i,
        proto = Object.prototype,
        ts = proto.toString,
        hasOwn = proto.hasOwnProperty.bind(obj);

    if ('[object Array]' !== ts.call(out)) out = [];

    for (i in obj) {
        if (hasOwn(i)) {
            if (i.toLowerCase() === key) {
                obj[replaceKey] = obj[i];
                delete obj[i];
            } else if ('[object Array]' === ts.call(obj[i]) || '[object Object]' === ts.call(obj[i])) {
                findAndReplaceProp(obj[i], key, replaceKey, out);
            }
        }
    }

    return obj;
}

function Rs(amount) {
    var words = new Array();
    words[0] = 'Zero'; words[1] = 'One'; words[2] = 'Two'; words[3] = 'Three'; words[4] = 'Four'; words[5] = 'Five'; words[6] = 'Six'; words[7] = 'Seven'; words[8] = 'Eight'; words[9] = 'Nine'; words[10] = 'Ten'; words[11] = 'Eleven'; words[12] = 'Twelve'; words[13] = 'Thirteen'; words[14] = 'Fourteen'; words[15] = 'Fifteen'; words[16] = 'Sixteen'; words[17] = 'Seventeen'; words[18] = 'Eighteen'; words[19] = 'Nineteen'; words[20] = 'Twenty'; words[30] = 'Thirty'; words[40] = 'Forty'; words[50] = 'Fifty'; words[60] = 'Sixty'; words[70] = 'Seventy'; words[80] = 'Eighty'; words[90] = 'Ninety'; var op;
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        var value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split(" ").join(" ");
    }
    return words_string;
}

function RsPaise(n) {
    var nums = n.toString().split('.');
    var whole = Rs(nums[0]);
    var op;
    if (nums[1] == null) nums[1] = 0;
    if (nums[1].length == 1) nums[1] = nums[1] + '0';
    if (nums[1].length > 2) { nums[1] = nums[1].substring(2, length - 1) }
    if (nums.length == 2) {
        if (nums[0] <= 9) { nums[0] = nums[0] * 10 } else { nums[0] = nums[0] };
        var fraction = Rs(nums[1])
        if (whole == '' && fraction == '') { op = 'Zero only'; }
        if (whole == '' && fraction != '') { op = 'paise ' + fraction + ' Only'; }
        if (whole != '' && fraction == '') { op = 'Rupees ' + whole + ' Only'; }
        if (whole != '' && fraction != '') { op = 'Rupees ' + whole + 'and paise ' + fraction + ' Only'; }
        return op;
    }
}

// Get Frequency in Integer
function getFrequencyInInt(freq) {
    switch (freq) {
        case 'M':
            return 1;
        case 'B':
            return 2;
        case 'Q':
            return 3;
        case 'H':
            return 6;
        case 'A':
            return 12;
        default:
            return;
    }
}

// Calculation of From & To Date for DayWise
function calculateProcessDateFilter(debitNoteData) {
    return new Promise((resolve, reject) => {
        if (!debitNoteData[0].DUEDAY) {
            reject('Dueday not found ...');
        }
        var dueDay = parseInt(debitNoteData[0].DUEDAY);
        var freq = getFrequencyInInt(debitNoteData[0].FREQUENCY);
        var CBDForDebitNote = moment(debitNoteData[0].CBD);
        var date = CBDForDebitNote.date();
        var month, fromDateForDaywise, toDateForDaywise;
        if (debitNoteData[0].FREQUENCY == "D") {
            fromDateForDaywise = moment(CBDForDebitNote).subtract(1, 'days').format('DD-MMM-YYYY');
            toDateForDaywise = moment(CBDForDebitNote).format('DD-MMM-YYYY');
        } else if (date > dueDay) {
            month = CBDForDebitNote.month();
            // To Date
            toDateForDaywise = moment().year(CBDForDebitNote.year()).month(month).date(dueDay).format('DD-MMM-YYYY');

            // From Date
            fromDateForDaywise = moment().year(CBDForDebitNote.year()).month(parseInt(month - freq)).date(dueDay + 1).format('DD-MMM-YYYY');
        } else {
            month = (CBDForDebitNote.month() - 1);

            // To Date
            toDateForDaywise = moment().year(CBDForDebitNote.year()).month(month).date(dueDay).format('DD-MMM-YYYY');

            // From Date
            fromDateForDaywise = moment().year(CBDForDebitNote.year()).month(parseInt(((dueDay + 1) > 31 ? month + 1 : month) - freq)).date(((dueDay + 1) > 31 ? 1 : (dueDay + 1))).format('DD-MMM-YYYY');
        }
        CBDForDebitNote = moment(CBDForDebitNote).format();
        resolve({
            fromDateForDaywise,
            toDateForDaywise,
            CBDForDebitNote
        })
    })
}

// Health API for Kubernetes
var healthApi = function (req, res) {
    res.status(200).send("Alive")
}

module.exports = {
    healthApi: healthApi,
    getInterestDebitNoteReport: getInterestDebitNoteReport
}