var log4js = require("log4js");


log4js.configure({
    levels: {
        INFO: { value: Number.MAX_VALUE, colour: 'blue' },
        OFF: { value: Number.MAX_VALUE - 1, colour: 'white' },
        AUDIT: { value: Number.MAX_VALUE, colour: 'yellow' },
        ERROR: { value: Number.MAX_VALUE, colour: 'red' }
    },
    appenders: {
        out: {
            type: 'console',
            "layout": {
                "type": "basic"
            }
        },

    },

    categories: { default: { appenders: ['out'], level: process.env.LOG_LEVEL || 'info' } }

});

var logger = log4js.getLogger();

module.exports = {
    logger: logger
}