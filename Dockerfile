FROM base-image:02
USER root

WORKDIR /app

COPY . .

EXPOSE 80

CMD node app.js
